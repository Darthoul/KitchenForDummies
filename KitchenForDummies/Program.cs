﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KitchenForDummies {
    class Program {
        static void Main (string[] args) {
            Ingredient ingredient = new Ingredient ();
            ingredient.flavor = new Flavor ();
            Console.WriteLine (ingredient.flavor.GetValue ("Sweer"));
            Ingredient tempIngredient = new Food (200, new KeyValuePair<string, float> ("Salty", 100));
            Console.WriteLine (tempIngredient.flavor.GetValue ("Sour"));
            Console.WriteLine (tempIngredient.flavor.GetValue ("Sweet"));
            Console.WriteLine (tempIngredient.flavor.GetValue ("Bitter"));
            Storage storage = new Storage ();
            storage.AddIngredient (ingredient);
            Console.WriteLine(storage.AddIngredient (tempIngredient));
        }
    }
}
