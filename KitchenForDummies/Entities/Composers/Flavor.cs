﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using KitchenForDummies.Utilities;

namespace KitchenForDummies {
    class Flavor {
        private const float PERCENT_EQUALIZE = 100;

        Dictionary<string, float> values;

        public Flavor () {
            values = new Dictionary<string, float> ();
            AddNewValue ("Salty");
            AddNewValue ("Sour");
            AddNewValue ("Sweet");
            AddNewValue ("Bitter");
            ConsoleDebug.LogWarning ("Creating a " + this + " with default values renders them equal. Use of AddToCurrentValues() expected");
        }

        private float AddNewValue (string valueName) {
            float currentPercent = PERCENT_EQUALIZE / (values.Count + 1);
            values.Add (valueName, currentPercent);
            SetKeyValues (SetDefaultPercent, 0f);
            return currentPercent;
        }

        public void AddToCurrentValues (string targetFlavor, float relativePercentage) {
            if (!values.Keys.Contains (targetFlavor)) {
                ConsoleDebug.LogError(this + " " + targetFlavor + " does not exist in the current context. Check for typos and Constructor's default Values");
                return;
            }

            float currentDifferential = PERCENT_EQUALIZE - values[targetFlavor];
            values[targetFlavor] += relativePercentage * (values[targetFlavor] / PERCENT_EQUALIZE);
            float targetDifferential = PERCENT_EQUALIZE - values[targetFlavor];

            SetKeyValues (SetEqualizedPercent, targetDifferential / currentDifferential, targetFlavor);
        }

        private void SetKeyValues (Func<string, float, float> valueMethod, float parameter, string ignoreValue = null) {
            List<string> tempKeys = new List<string> ();
            foreach (KeyValuePair<string, float> percentValue in values) {
                tempKeys.Add (percentValue.Key);
            }
            foreach (string tempListKey in tempKeys) {
                if (tempListKey != ignoreValue) {
                    valueMethod (tempListKey, parameter);
                }
            }
        }

        private float SetDefaultPercent (string currentKey, float parameter) {
            values[currentKey] = PERCENT_EQUALIZE / values.Count;
            return values[currentKey];
        }
        private float SetEqualizedPercent (string currentKey, float parameter) {
            values[currentKey] *= parameter;
            return values[currentKey];
        }

        public float GetValue (string targetFlavor) {
            if (!values.Keys.Contains (targetFlavor)) {
                ConsoleDebug.LogError (this + " " + targetFlavor + " does not exist in the current context. Check for typos and Constructor's default Values");
                return 0;
            }
            return values[targetFlavor];
        }
    }
}
