﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using KitchenForDummies.Utilities;

namespace KitchenForDummies {
    class Food : Ingredient {

        public Food (float _grams, KeyValuePair<string,float> _flavorAmmount) {
            grams = _grams;
            flavor = new Flavor ();
            flavor.AddToCurrentValues (_flavorAmmount.Key, _flavorAmmount.Value);
            ConsoleDebug.Log (this + " created with target " + flavor + " " + _flavorAmmount.Key + " set at " + _flavorAmmount.Value + " and currently at " + flavor.GetValue (_flavorAmmount.Key));
        }
    }
}
