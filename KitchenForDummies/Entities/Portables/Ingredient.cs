﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KitchenForDummies {
    class Ingredient {
        protected string name;
        public string Name { get { return name; } }
        protected float grams;
        public float Grams { get { return grams; } }
        public Flavor flavor;

        public Ingredient () {
            name = "";
            grams = 1;
        }

        
    }
}
