﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KitchenForDummies {
    class Storage {
        readonly List<Ingredient> ingredients = new List<Ingredient> ();

        public T AddIngredient<T> (T ingredient) where T: Ingredient {
            Console.WriteLine ("DEBUG: Type of " + ingredient + " is " + typeof (T));
            return ingredient;
        }
    }
}
