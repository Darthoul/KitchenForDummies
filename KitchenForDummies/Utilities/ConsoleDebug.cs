﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KitchenForDummies.Utilities {
    static class ConsoleDebug {

        private const string LOG_HEADER = "LOG: ";
        private const string WARNING_HEADER = "WARNING: ";
        private const string ERROR_HEADER = "ERROR: ";

        static public string Log (string message) {
            return ComposeMessage (LOG_HEADER, message, ConsoleColor.Gray);
        }
        static public string LogWarning (string message) {
            return ComposeMessage (WARNING_HEADER, message, ConsoleColor.Yellow);
        }
        static public string LogError (string message) {
            return ComposeMessage (ERROR_HEADER, message, ConsoleColor.Red);
        }

        static string ComposeMessage (string header, string message, ConsoleColor color) {
            ConsoleColor tempColor = Console.ForegroundColor;
            Console.ForegroundColor = color;
            string composedMessage = header + message;
            Console.WriteLine (composedMessage);
            Console.ForegroundColor = tempColor;
            return composedMessage;
        } 
    }
}
